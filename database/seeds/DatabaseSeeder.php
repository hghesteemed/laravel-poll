<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Model::unguard();
        $this->call('PollDataSeeder');
        $this->command->info('Poll Tables seeded!');
        // $this->call(UserTableSeeder::class);

        Model::reguard();
    }

}
