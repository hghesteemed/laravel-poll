<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('api/poll','PollsController');
Route::get('list','PollsController@listing');
Route::resource('api/polloption','PollOptionsController');
Route::get('api/polloption/addvote/{id}','PollOptionsController@addVote');
