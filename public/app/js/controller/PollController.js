app.controller('pollController', function ($scope, $http) {

    $scope.polls = [];

    $http.get('/api/poll').
            success(function (data, status, headers, config) {
                $scope.polls = data;
            });

    $scope.addVote = function (polloptions) {
        $http.get('/api/polloption/addvote/' + polloptions.id).
                success(function (data, status, headers, config) {
                    polloptions.vote++;
                });
    }

});

app.controller('adminController', function ($scope, $http) {

    $scope.polls = [];
    $scope.newpoll = {};

    $http.get('/api/poll').
            success(function (data, status, headers, config) {
                $scope.polls = data;
            });

    $scope.addPoll = function () {
        $http.post('/api/poll', $scope.newpoll).
                success(function (data, status, headers, config) {
                    console.log("Added new poll!");
                    console.log(data);
                    data.polloptions = [];
                    $scope.polls.push(data);
                    $scope.newpoll.title = "";
                });
    }

    $scope.removePoll = function (poll) {
        $http.delete('/api/poll/' + poll.id).
                success(function (data, status, headers, config) {
                    for (index = 0; index < $scope.polls.length; ++index) {
                        if ($scope.polls[index].id == poll.id) {
                            $scope.polls.splice(index, 1);
                        }
                    }
                });
    }

    $scope.addOption = function (poll, newoption) {
        newoption.poll_id = poll.id;
        $http.post('/api/polloption', newoption).
                success(function (data, status, headers, config) {
                    poll.polloptions.push(data);
                    newoption.title = '';
                });
    }

    $scope.removeOption = function (polloptions, poll) {
        $http.delete('/api/polloption/' + polloptions.id).
                success(function (data, status, headers, config) {
                    for (index = 0; index < poll.polloptions.length; ++index) {
                        if (poll.polloptions[index].id == polloptions.id) {
                            poll.polloptions.splice(index, 1);
                        }
                    }
                });
    }
});

